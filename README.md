# Simple Explorer

Simple Explorer is a console application written in Java that provides various directory and file related operations to the user. The program can **read** ASCII files, **copy**, **move** etc. as well as visually draw the directory.  Along these, it can also perform **searches** using two different methods that fit different scenarios.

## How to use

When the program is launched, it will draw the Main Menu which will consist of menu options that the user can pick from. Each number on the menu entry corresponds to its index, meaning that entering that index will enter that menu.
  

# UI System

The UI system was inspired general binary trees. Each menu has a name, and child nodes that are menus as well. The user can use the UI class to dynamically add new menu entries to the program. After the new menus are created, each menu entry can be bound to a specific function which will be executed when that menu is accessed.

# File System

The file system can only properly read ASCII files. It *won't* prevent you opening from any binary data, but keep in mind that it won't be able to print the data. However, other operations such as move, copy and delete will function as normal. 

# Logger

The program has a logging system in place that helps keep track of every operation that occurs in the main system. Every action will be logged to both the console and a log file in  


# Program flow

There are few classes in this project and not much is happening under the hood. The flow of the program can be represented with a flow chart. It would look something like this.

```mermaid
graph LR
Main[Draw Menu] --> B[Get Input]
B[Get Input] --> C[Check Input]
C[Check Input] --> D[Valid]
C[Check Input] --> E[Invalid]
D[Valid] --> F[Get Function Input]
E[Invalid] --> Main[Draw Menu]
F[Get Function Input] --> G[Check Function Input]
G[Check Function Input] --> H[Valid]
G[Check Function Input] --> I[Invalid]
I[Invalid] --> Main[Draw Menu]
H[Valid] --> J[Execute Function]
```
