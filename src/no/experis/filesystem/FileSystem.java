package no.experis.filesystem;

import no.experis.logger.Logger;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileSystem
{
    public enum SORT {ALL, FILES, FOLDERS, EXTENSION}
    private File file;
    private String currentPath;
    private String data; // Holds our text data from the file
    private Logger logSystem = null;
    public FileSystem()
    {
        currentPath =  System.getProperty("user.dir");
        logSystem = new Logger();
    }
    public FileSystem(String initialPath)
    {
        currentPath = initialPath;
    }
    public Logger getLogSystem()
    {
        return logSystem;
    }
    // Creates a new text file inside the current directory
    public void newFile(Scanner console)
    {
        System.out.print("Enter file name: ");
        String fileName = console.next();
        console.nextLine();
        System.out.print("Enter file data: ");
        String fileData = console.next();
        console.nextLine();
        logSystem.start();
        try
        {
            FileWriter writer = new FileWriter(currentPath + "\\" + fileName + ".txt");
            writer.write(fileData);
            writer.close();
            logSystem.end();
            logSystem.log("Successfully created file '" + fileName + ".txt' in " + currentPath + ".");
        }
        catch (IOException e)
        {
            logSystem.end();
            logSystem.log(e.getMessage());
        }
    }
    // Opens a file, reads every line one by one and stores it inside the data variable
    public void openFile(Scanner console)
    {
        System.out.print("Enter file name: ");
        String fileName = console.next();
        logSystem.start();
        try (Scanner fileReader = new Scanner(file = new File(currentPath + "\\" + fileName)))
        {
            System.out.println("Reading file...");
            while(fileReader.hasNext())
                data += fileReader.nextLine() + "\n";
            currentPath = file.getParent();
            logSystem.end();
            logSystem.log("Successfully read '" + file.getName() + "'.");
        }
        catch (IOException e)
        {
            logSystem.end();
            logSystem.log(e.getMessage());
        }
    }
    public void toggleLogSystem(boolean enabled)
    {
        logSystem.log("Console logging has been " + ((enabled) ? "enabled." : "disabled."));
        logSystem.setPrintToConsole(enabled);
    }
    // Deletes a file as long as the program has the permissions to do so
    public void deleteFile(Scanner console)
    {
        logSystem.start();
        if (file == null)
        {
            logSystem.end();
            logSystem.log("No open file to perform a deletion operation!");
            return;
        }
        while (true)
        {
            System.out.print("Are you sure you want to delete this file? (y/n): ");
            char response = console.next().charAt(0);   // Only registers the first character
            logSystem.start();
            if (response == 'y')
            {
                try
                {
                    file.delete();
                    file = null;
                    logSystem.end();
                    logSystem.log("Successfully deleted file.");
                }
                catch (SecurityException e)
                {
                    logSystem.end();
                    logSystem.log(e.getMessage());
                }
                break;
            }
            else if (response == 'n')
            {
                logSystem.end();
                logSystem.log("File delete operation cancelled.");
                break;
            }
        }
    }
    // Closes the current file and nullifies the file reference
    public void closeFile()
    {
        logSystem.start();
        if (file == null)
        {
            logSystem.end();
            logSystem.log("No open file to close!");
        }
        else
        {
            file = null;
            data = null;
            logSystem.end();
            logSystem.log("File closed successfully.");
        }
    }
    public void printFile()
    {
        logSystem.start();
        if (file == null)
        {
            logSystem.end();
            logSystem.log("No open file to print!");
            return;
        }
        System.out.println("         Print Start       ");
        System.out.println("──────────────────────────────\n");
        System.out.println(data);
        System.out.println("──────────────────────────────");
        System.out.println("         Print End       ");
        logSystem.end();
        logSystem.log("Successfully printed '" + file.getName() + "'.");
    }

    // Changes the currently working directory if it exists
    public void changeCurrentPath(Scanner console)
    {
        System.out.print("Enter new directory: ");
        String path = console.next();
        logSystem.start();
        if (new File(path).exists())
        {
            logSystem.end();
            logSystem.log("Successfully changed directory.");
            this.currentPath = path;
        }
        else
        {
            logSystem.end();
            logSystem.log("Can't change to the specified directory because it does not exist!");
        }
    }
    public String getCurrentPath()
    {
        return currentPath;
    }
    public String getData()
    {
        return data;
    }
    public String getPath()
    {
        logSystem.start();
        logSystem.end();
        logSystem.log("Successfully retrieved current directory.");
        return " Directory: " + currentPath;
    }
    // Retrieves the name of the current file
    public String getFile()
    {
        logSystem.start();
        if (file != null || data != null)
        {
            logSystem.end();
            logSystem.log("Successfully retrieved file name.");
            return " File: " + file.getName();
        }
        else
        {
            logSystem.end();
            logSystem.log("No open file to display a file name!");
            return null;
        }
    }
    // Retrieves details about the current file
    public String getDetails()
    {
        logSystem.start();
        if (file == null || data.length() == 0)
        {
            logSystem.end();
            logSystem.log("No open file to display information!");
            return null;
        }
        String result = " Name: " + file.getName() + "\n Size: " + (float)(file.length() / 1000.f) + " KB" + "\n Lines: " + data.lines().count() + "\n Modified: " + new Date(file.lastModified()).toString();
        logSystem.end();
        logSystem.log("Successfully retrieved information about a file.");
        return result;
    }
    // Iterates through the directory and retrieves all file extensions
    public List<String> getAllExtensions(File[] files)
    {
        List<String> extensions = new ArrayList<String>(); // Stores all found extensions inside an array
        for (File one : files)
        {
            if(one.isFile()) // Making sure it's a file and not a directory
            {
                String fileName = one.toString();

                int index = fileName.lastIndexOf('.'); // Get the correct index where the extension starts
                if(index > 0)
                {
                    String extension = fileName.substring(index + 1);
                    if (extensions.contains(extension) == false) // Check if the extensions has already been added, if not it's pushed to the array
                        extensions.add(extension);
                }
            }
        }
        return extensions;
    }
    // Sorts the way the directory is displayed. Each case iterates through the directory to retrieve the correct files/folders
    public String sort(SORT option, Scanner console)
    {
        logSystem.start();
        String result = "";
        File dir = new File(currentPath);
        var files = dir.listFiles(); // Get everything inside the directory
        switch (option)
        {
            case ALL: // This will collect everything
            {
                for (File one : files)
                {
                    if (one.isFile()) result += " ░ " + one.getName() + "\n";
                    else result += " █ " + one.getName() + "\n";
                }
                logSystem.end();
                logSystem.log("Sorted directory by ALL.");
                break;
            }
            case FILES: // This collect only files
            {
                for (File one : files)
                    if (one.isFile())
                        result += " ░ " + one.getName() + "\n";
                logSystem.end();
                logSystem.log("Sorted directory by FILES.");
                break;
            }
            case FOLDERS: // This will collect only folders
            {
                for (File one : files)
                    if (one.isDirectory())
                        result += " █ " + one.getName() + "\n";
                logSystem.end();
                logSystem.log("Sorted directory by FOLDERS.");
                break;
            }
            case EXTENSION: // This will only collect files with the specified extension
            {
                var extensions = getAllExtensions(files); // First get all of the available extensions
                System.out.print("Available extensions: " );
                extensions.forEach(item -> { System.out.print(item + ", "); }); // Print to the user all the extensions found
                System.out.print("\b\b\nEnter extension: " );
                String desiredExtension = console.next();
                logSystem.start();
                var sortedFiles = dir.listFiles((dir1, name) -> name.toLowerCase().endsWith("." + desiredExtension)); // Sorts so it only gets the files with the specified extension
                if (sortedFiles.length > 0)
                {
                    for (File one : sortedFiles) // Iterates through the sorted files
                        if (one.isFile())   // and making sure it's actually a file
                            result += " ░ " + one.getName() + "\n";
                    logSystem.end();
                    logSystem.log("Sorted directory by EXTENSION.");
                }
                else
                {
                    logSystem.end();
                    logSystem.log("No files found with that extension!"); // If no files are found with that extension, It logs an error
                    return null;
                }
                break;
            }
        }
        if (result.length() > 0) return result.substring(0,result.length()-1); // Checks if result string has characters, otherwise accessing it will crash the program
        else return null;
    }
    // Uses a bog standard naive approach to find matches inside a string. Reads line by line to find occurrences of a pattern.
    // This method is usually found to be faster on small files that contain little to very little text.
    public String findStandard(Scanner console, boolean caseSensitive)
    {
        logSystem.start();
        if (file == null || data == null)
        {
            logSystem.end();
            logSystem.log("No open file to perform a search!");
            return null;
        }
        System.out.print("Search in file: ");
        String word = console.next();
        int occurrences = 0;
        String line = "";
        System.out.println("Searching...");
        logSystem.start();
        BufferedReader bufReader = new BufferedReader(new StringReader(data)); // A bufferedReader is used to read the string line by line
        try
        {
            if (caseSensitive)
            {
                while((line = bufReader.readLine()) != null )   // Making sure that the line it's reading isn't empty
                {
                    if (line.contains(word)) // If the line contains the word we need, 'occurrences' variable is increased
                        occurrences++;
                }
                logSystem.end();
                logSystem.log("Performed standard case sensitive search for word '" + word + "' and returned " + occurrences + " results.");
            }
            else
            {
                while((line = bufReader.readLine()) != null )
                {
                    if (line.toLowerCase().contains(word.toLowerCase()))    // To make sure the search is non-case sensitive, both line and word are converted to lower case
                        occurrences++;
                }
                logSystem.end();
                logSystem.log("Performed standard non-case sensitive search for word '" + word + "' and returned " + occurrences + " results.");
            }
        }
        catch (IOException e)
        {
            logSystem.end();
            logSystem.log(e.getMessage());
            return " Error while searching!";
        }
        return " The word '" + word + "' was found " + occurrences + " time(s)";
    }
    // A faster approach for searching patterns in strings.
    // Rather than using a naive brute force algorithm for searching, it uses a pre-compiled constant pattern to perform a search.
    // The pattern is processed using a Boyer-Moore algorithm, rather than the built-in regex engine
    // which provides better efficiency since the regex engine is slow.
    // This method is found to have better performance than naive searching on larger files
    // that contain a lot of text
    public String findBoyerMoore(Scanner console, boolean caseSensitive)
    {
        logSystem.start();
        if (file == null || data == null)
        {
            logSystem.end();
            logSystem.log("No open file to perform a search!");
            return null;
        }
        System.out.print("Search in file: ");
        String word = console.next();
        int occurrences = 0;
        System.out.println("Searching...");
        logSystem.start();
        if (caseSensitive)
        {
            Pattern pattern = Pattern.compile(word);
            Matcher matcher = pattern.matcher(data);
            while (matcher.find())
                occurrences++;

            logSystem.end();
            logSystem.log("Performed Boyer-Moore case sensitive search for word '" + word + "' and returned " + occurrences + " results.");
        }
        else
        {
            Pattern pattern = Pattern.compile(word.toLowerCase());
            Matcher matcher = pattern.matcher(data.toLowerCase());
            while (matcher.find())
                occurrences++;

            logSystem.end();
            logSystem.log("Performed Boyer-Moore non-case sensitive search for word '" + word + "' and returned " + occurrences + " results.");
        }
        return " The word '" + word + "' was found " + occurrences + " time(s)";
    }
    public void moveFile(Scanner console)
    {
        if (file == null || data == null)
        {
            logSystem.end();
            logSystem.log("No open file to move!");
            return;
        }
        System.out.print("Enter new destination: ");
        String dest = console.next();
        if (new File(dest).exists() == false)
        {
            logSystem.end();
            logSystem.log("Failed to move file because an invalid destination was entered!");
            return;
        }
        logSystem.start();
        try
        {
            file.renameTo(new File(dest + "\\" + file.getName()));
        }
        catch (SecurityException e)
        {
            logSystem.end();
            logSystem.log(e.getMessage());
        }
        finally
        {
            File newFile = new File(dest + "\\" + file.getName());
            file.delete();
            file = newFile;
            currentPath = dest;
        }
        logSystem.end();
        logSystem.log("Successfully moved file '" + file.getName() + "' to " + dest);
    }
    public void copyFile(Scanner console)
    {
        if (file == null || data == null)
        {
            logSystem.end();
            logSystem.log("No open file to copy!");
            return;
        }
        System.out.print("Enter new destination: ");
        String dest = console.next();
        if (new File(dest).exists() == false)
        {
            logSystem.end();
            logSystem.log("Failed to copy file because an invalid destination was entered!");
            return;
        }
        logSystem.start();
        try
        {
            file.renameTo(new File(dest + "\\" + file.getName() + " - Copy"));
            currentPath = dest;
        }
        catch (SecurityException e)
        {
            logSystem.end();
            logSystem.log(e.getMessage());
        }
        logSystem.end();
        logSystem.log("Successfully copied file '" + file.getName() + "' to " + dest);
    }
}
