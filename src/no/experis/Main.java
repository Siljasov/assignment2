package no.experis;
import no.experis.ui.UI;
import no.experis.filesystem.FileSystem;

public class Main
{
    // Dynamically adding menu entries using UI class
    // The first parameter searches for the parent menu name, while the second adds a new menu with that name
    public static void initializeMenus(UI menu)
    {
        menu.addMenu("Simple Explorer", "Directory");
        menu.addMenu("Simple Explorer", "File");
        menu.addMenu("Simple Explorer", "Settings");
        menu.addMenu("Directory", "Current directory");
        menu.addMenu("Directory", "Change");
        menu.addMenu("Directory", "Draw");
        menu.addMenu("Directory", "Filter");
        menu.addMenu("Filter", "Sort by file");
        menu.addMenu("Filter", "Sort by folder");
        menu.addMenu("Filter", "Sort by extension");
        menu.addMenu("Directory", "Open file");
        menu.addMenu("Directory", "Create file");
        menu.addMenu("File", "Currently open file");
        menu.addMenu("File", "Print");
        menu.addMenu("File", "Find");
        menu.addMenu("File", "Copy");
        menu.addMenu("File", "Move");
        menu.addMenu("File", "Delete");
        menu.addMenu("File", "Details");
        menu.addMenu("File", "Close");
        menu.addMenu("Find", "Naive algorithm");
        menu.addMenu("Find", "Boyer-Moore algorithm");
        menu.addMenu("Naive algorithm", "Naive Case sensitive");
        menu.addMenu("Naive algorithm", "Naive Not case sensitive");
        menu.addMenu("Boyer-Moore algorithm", "Fast Case sensitive");
        menu.addMenu("Boyer-Moore algorithm", "Fast Not case sensitive");
        menu.addMenu("Settings", "Enable console logs");
        menu.addMenu("Settings", "Disable console logs");
        menu.addMenu("Settings", "About");
    }
    // Adds functions to specific menu selections by using lambda expressions
    // Each menu entry has a variable called 'func' which is a Runnable. 'func' holds a reference to a function which is run when that particular menu is accessed
    // addFunction() adds functionality to the menu, while addTextToMenu() adds some sort of text on top of the added functionality.
    // Explanation: addFunction( "Menu name here", () -> { "what we want to run when the menu is accessed" } );
    // Example 1: addFunction("Print", () -> { print("This text will be printed to the console"); } );
    // Example 2: addFunction("Our Menu", () -> { addTextToMenu("Our Menu", "This text will now be shown inside the UI when this menu is accessed"); } );
    public static void initializeFunctions(UI menu, FileSystem system)
    {
        menu.addFunction("Change", () -> {system.changeCurrentPath(menu.getConsole()); });
        menu.addFunction("Open file", () -> {system.openFile(menu.getConsole()); });
        menu.addFunction("Create file", () -> {system.newFile(menu.getConsole()); });
        menu.addFunction("Print", () -> {system.printFile(); });
        menu.addFunction("Copy", () -> {system.copyFile(menu.getConsole()); });
        menu.addFunction("Move", () -> {system.moveFile(menu.getConsole()); });
        menu.addFunction("Delete", () -> {system.deleteFile(menu.getConsole()); });
        menu.addFunction("Details", () -> { menu.addTextToMenu("Details", system.getDetails()); });
        menu.addFunction("Draw", () -> { menu.addTextToMenu("Draw", system.sort(FileSystem.SORT.ALL, menu.getConsole())); });
        menu.addFunction("Sort by folder", () -> { menu.addTextToMenu("Sort by folder", system.sort(FileSystem.SORT.FOLDERS, menu.getConsole())); });
        menu.addFunction("Sort by file", () -> { menu.addTextToMenu("Sort by file", system.sort(FileSystem.SORT.FILES, menu.getConsole())); });
        menu.addFunction("Sort by extension", () -> { menu.addTextToMenu("Sort by extension", system.sort(FileSystem.SORT.EXTENSION, menu.getConsole())); });
        menu.addFunction("Current directory", () -> { menu.addTextToMenu("Current directory", system.getPath()); });
        menu.addFunction("Currently open file", () -> { menu.addTextToMenu("Currently open file", system.getFile()); });
        menu.addFunction("Close", () -> {system.closeFile(); });
        menu.addFunction("Naive Case sensitive", () -> { menu.addTextToMenu("Naive Case sensitive", system.findStandard(menu.getConsole(),true)); });
        menu.addFunction("Naive Not case sensitive", () -> { menu.addTextToMenu("Naive Not case sensitive", system.findStandard(menu.getConsole(),false)); });
        menu.addFunction("Fast Case sensitive", () -> { menu.addTextToMenu("Fast Case sensitive", system.findBoyerMoore(menu.getConsole(),true)); });
        menu.addFunction("Fast Not case sensitive", () -> { menu.addTextToMenu("Fast Not case sensitive", system.findBoyerMoore(menu.getConsole(),false)); });
        menu.addFunction("Enable console logs", () -> {system.toggleLogSystem(true); });
        menu.addFunction("Disable console logs", () -> {system.toggleLogSystem(false); });
        menu.addTextToMenu("About", " Simple Explorer\n A program that performs various\n file and directory operations.\n Made by Sultan Iljasov.");
    }
    public static void main(String[] args)
    {
        boolean loop = true;
        UI menu = new UI("Simple Explorer");
        FileSystem system = new FileSystem();
        initializeMenus(menu);
        initializeFunctions(menu, system);
        while(loop)
        {
            menu.print();
            loop = menu.getInput();
        }
    }
}
