package no.experis.logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

public class Logger
{
    private long start = 0;
    private long end = 0;
    private long duration = 0;
    private String path;
    private boolean printToConsole = true;
    public Logger()
    {
        path = System.getProperty("user.dir") + "\\src\\logs\\";
        try
        {
            File dir = new File(path); // Attempting to open the log file
            if (dir.exists() == false) // If the file does not exist, it's created
            {
                dir.mkdirs();
                File file = new File(path + "systemlog.txt");

                if (file.exists() == false) file.createNewFile();
            }
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
    // Simple function to force the thread to sleep for a specific amount of time
    private void wait(int time)
    {
        try
        {
            Thread.sleep(time * 1000);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }
    // Sets the starting point to calculate execution time later
    public void start()
    {
        start = System.nanoTime();
    }
    // Sets the ending point to calculate execution time later
    public void end()
    {
        end = System.nanoTime();
        duration = end - start;
    }
    public long getNanoseconds()
    {
        return duration;
    }
    public float getMilliseconds()
    {
        return (duration / 1000000.f);
    }
    // Logs relevant information to both the console and systemlog file
    public void log(String message)
    {
        String log = Calendar.getInstance().getTime().toString() + ": " + message + " Executed in " + String.format("%.2f", getMilliseconds()) + " ms.\n";
        if (printToConsole) // if the option to print to console is enabled, it's printed there as well
        {
            System.out.println(log);
            wait(2);
        }
        save(log); // Save the log
        reset(); // and reset the variables
    }
    public void reset()
    {
        duration = 0;
        start = 0;
        end = 0;
    }
    // Opens the log file, creates a new line and saves it
    private void save(String data)
    {
        try
        {
            FileWriter writer = new FileWriter(path + "systemlog.txt", true);
            writer.write(data);
            writer.close();
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
    public boolean isPrintToConsole()
    {
        return printToConsole;
    }

    public void setPrintToConsole(boolean printToConsole)
    {
        this.printToConsole = printToConsole;
    }
}
