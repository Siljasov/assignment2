package no.experis.ui;

import java.util.List;
import java.util.ArrayList;

public class UICategory
{
    List<UICategory> children;
    String name;
    UICategory parent;
    Runnable func;
    String text;
    UICategory(String categoryName)
    {
        children = new ArrayList<UICategory>();
        name = categoryName;
    }
    public UICategory findCategory(String findName, UICategory current)
    {
        if (current.name.equals(findName)) // If we find ourselves in the correct menu selection, we return the current reference
            return current;
        for (UICategory child : current.children) // Iterate through all of this menu's children
        {
            UICategory result = child.findCategory(findName, child);    // Recursively check all of them to see if they contain the correct menu selection name we need
            if (result != null) return result;
        }
        return null;
    }
    // Creates a new child menu selection for this menu
    public boolean add(String subMenuName)
    {
        UICategory newMenu = new UICategory(subMenuName);
        newMenu.parent = this;
        children.add(newMenu);
        return true;
    }
}
