package no.experis.ui;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UI
{
    private Scanner console;
    private UICategory current; // Holds the reference to where we are currently in the menu tree
    private UICategory root; // Holds a constant reference to the root menu
    public UI()
    {
        console = new Scanner(System.in);
        current = new UICategory("Main Menu");
        root = current;
    }
    public UI(String initialMenuName)
    {
        console = new Scanner(System.in);
        current = new UICategory(initialMenuName);
        root = current;
    }

    // Adds a new menu entry under a category. For example, inCategory: "Main Menu" and subMenuName "New file"
    // will search for a category called "Main Menu" and add "New file" inside its children array
    public boolean addMenu(String inCategory, String subMenuName)
    {
        var newCategory = root.findCategory(inCategory,root);
        if (newCategory == null) return false;
        else
        {
            newCategory.add(subMenuName);
            return true;
        }
    }
    // Does practically the same thing as addMenu, except it adds text to a menu
    // that will be displayed inside the UI when the menu is accessed
    public boolean addTextToMenu(String menuName, String text)
    {
        var newCategory = root.findCategory(menuName,root);
        if (newCategory == null) return false;
        else
        {
            newCategory.text = text;
            return true;
        }
    }
    // Sleeps the thread for some amount of time
    private void wait(int time)
    {
        try
        {
            Thread.sleep(time * 1000);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }
    // Retrieves the longest line that will be present in the UI so the system can evenly place walls later
    private int getLongestLine(String text)
    {
        int result = 0;
        String[] splitStr = text.split("\n");
        for (String one : splitStr)
        {
            if (one.length() > result) result = one.length();
        }
        return result;
    }
    // Makes the UI walls evenly placed in regards to how long the current line is
    private String createWall(int length, char symbol)
    {
        String result = "";
        for (int i = 0; i < length; i++) result += symbol;
        return result;

    }
    // Does essentially the same thing as the function above with one difference.
    // It takes in an array of strings and places a wall accordingly.
    private String createWallFunctionText(int length, String[] text)
    {
        String result = "";
        for (String one : text)
        {
            result += "║" + one + createWall(length - one.length(),' ') + " ║\n";
        }
        return result;
    }
    public void print()
    {
        int longestLine = 30;   // We start with a default width for our walls
        if (current.text != null)   // If a function text is being displayed, we use that as the new width for the line
            if (getLongestLine(current.text) > 30)    // as long as it is longer than our default width
                longestLine = getLongestLine(current.text);

        System.out.println("╔" + createWall(longestLine,'═') + "═╗");
        System.out.println("║ " + current.name + createWall(longestLine - current.name.length()-1,' ') + " ║");
        System.out.println("╠" + createWall(longestLine,'═') + "═╣");
        if (current.children.size() > 0)
        {
            for (int i = 0; i < current.children.size(); i++) // If the menu has children, then it's a category and its children menus are printed
            {
                System.out.println("║ " + (i+1) + ". " + current.children.get(i).name + createWall(longestLine - current.children.get(i).name.length()-4, ' ') + " ║");
            }
        }
        else if (current.text != null) // If the menu has text, it's displayed inside the UI
        {
            System.out.print(createWallFunctionText(longestLine, current.text.split("\n")));
        }
        System.out.println("╠" + createWall(longestLine,'═') + "═╣");
        System.out.println("║ 0. Back" + createWall(longestLine-8,' ') + " ║");
        System.out.println("║ -1. Exit" + createWall(longestLine-9,' ') + " ║");
        System.out.println("╚" + createWall(longestLine,'═') + "═╝");
    }
    // Sets the Runnable func for a particular menu selection
    public boolean addFunction(String menuName, Runnable function)
    {
        var menu = root.findCategory(menuName,root);
        menu.func = function;
        return true;
    }
    // Handles user input and interaction
    public boolean getInput()
    {
        byte input;
        try {
            System.out.print("Input: ");
            input = console.nextByte();
            if (input <= current.children.size() && input > 0) // We check if the user has entered a valid menu index
            {
                current = current.children.get(input-1); // Current reference now points to the corresponding child menu
                if (current.children.size() == 0 && current.func != null) // If the current menu selection has no children and a function bounded to it
                {                                                         // we run it
                    current.func.run();
                    if (current.text == null)   // If the current menu selection has no text, current is set back to its parent menu
                        current = current.parent;
                }
                return true;
            }
            else if (input == 0 && current.parent != null) // If user enters 0, they are sent back to the previous menu
            {
                current = current.parent;
                return true;
            }
            else if (input == -1) // Entering -1 will quit the program
                return false;
            else // If the entered value is none of the above, it must be invalid
            {
                System.out.println("Invalid input!\n");
                wait(3);
            }
        } catch (InputMismatchException e)
        {
            System.out.println("Invalid input!\n");
            wait(3);
        };
        console.nextLine();
        return true; // Return true here because otherwise Java will complain
    }
    public Scanner getConsole()
    {
        return console;
    }
}
